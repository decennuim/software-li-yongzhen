import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

public class BankService extends  Thread implements Runnable {

    // 客户等待队列
    public LinkedBlockingQueue<Customer> waitQueue;
    // 服务线程监控
    CountDownLatch latch;
    // 服务窗口数
    public int seriveNum;
    // 客户处理完成后进入汇总集合
    public List<Customer> collectList;
    // 服务窗口线程组,响应中断用
    ThreadGroup tg;

    public BankService() {

// 只有200把椅子
        this.waitQueue = new LinkedBlockingQueue<>(200);
// 默认4个服务窗口
        this.seriveNum = 4;
        latch = new CountDownLatch(seriveNum);
// 初始化汇总集合为线程安全集合
        this.collectList = Collections.synchronizedList(new ArrayList<>());
// 默认为true 结束为false
// this.flowFlag = true;
        tg = new ThreadGroup("服务窗口线程组");
    }

    public BankService(int seriveNum) {
        this();
        this.seriveNum = seriveNum;
        latch = new CountDownLatch(seriveNum);
    }
    /**
     * 模拟处理过程
     *
     * @param
     */
    private void handle(List<Service> services) {

        for (Service s : services) {
            try {
                Thread.sleep((long) (Math.random() * 100));
                Bank2022.now++;

            } catch (InterruptedException e) {
// 这里模拟过程用了sleep，可能被中断打断，现实中是不可能的
            }
        }
    }

    /**
     * 客户到达银行，入队
     *
     * @param
     */
    public void come(List<Customer> customers) { //list
        new Thread(() -> {
            System.out.println("银行大门打开，开始排队：");

            for (Customer c : customers) {

                try {
// 假设500毫秒进来一个客户
                    Thread.sleep(500);
                    waitQueue.put(c); //放入等待队列
                   Bank2022.total++;
                    System.out.println("客户：" + c + "进入等待队列");
                } catch (InterruptedException e) {
// TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            System.out.println("客户全部被服务");
// flowFlag = false;
        }).start();
    }

    /**
     * 服务主方法
     */
    public void service() {
// 窗口服务结果

        for (int i = 0; i < seriveNum; i++) {
// 窗口号默认按照线程执行顺序
            final int n = i + 1;
            final Counter counter = new Counter();
            new Thread(tg, String.valueOf(n)) {
                public void run() {
                    int count=0;
                    try {
                        System.out.println("窗口：【" + n + " 】开始工作");
// 从等待队列中提取需待服务客户
                   //Bank2022 bank2022=new Bank2022();
                        Customer c;
                        while (true) {
                            try {
                                c = waitQueue.take();

                            //   bank2022.total++;   //将每一个进入队列的与信息一一对应

                            } catch (InterruptedException e) {
                                System.out.println("窗口：【" + n + " 】终止服务");
                                break;
                            }
                            c.setCounter(n);
// 模拟处理过程
                            handle(c.getServices());
// 窗口统计
                            counter.inc(c.getServices());
// 处理完成设置完成时间
                            c.setFinshTime(c.enterTime+c.needtime);
// 将客户移到汇总集合中
                            collectList.add(c);

                            System.out.println(
                                    "窗口：【" + n + " 】完成了:编号【" + c.getNum() + "】的客户业务工作");
                              //  Bank2022.now++;
// 释放掉引用
                            c = null;
                        }
// 完成工作
                        String st=counter.toStr();
                        System.out.println("窗口：【" + n + " 】完成工作" + st);
                        if (n==1){
                        frame3.s1="窗口:["+n+"]完成工作"+st;
                        count++;
                        }else if (n==2){
                            frame3.s2="窗口:["+n+"]完成工作"+st;
                            count++;
                        }else if (n==3){
                            frame3.s3="窗口:["+n+"]完成工作"+st;
                            count++;
                        }else if (n==4){
                            frame3.s4="窗口:["+n+"]完成工作"+st;
                            count++;
                        }

                        //根据count++判断,count=1进s1,2进s2,3进s3
                    } finally {
                        latch.countDown();
                    }
                }
            }.start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    void serviceInterrput(int workTime) {
        Thread t = new Thread() {
            @SuppressWarnings("unused")
            private int wkt;
            public void run() {
                this.wkt = workTime;
                int jbTime = 1000;
                while(true) {
// 到下班时间检查队列是否为空，如果是，就结束
                    try {
                        Thread.sleep(workTime);
                    } catch (InterruptedException e) {
// TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if (waitQueue.isEmpty()) {
                        tg.interrupt();
                        break;
                    }
                    wkt = jbTime;
                }
            }
        };
        t.setDaemon(true);
        t.start();
    }


    /**
     * 客户平均逗留时间
     *
     * @return
     */
    public int getAvgTime() {
        int time =0;
      //  int time1=0;
        for (Customer c : collectList) {

            time+=(int)(Math.random()*50);
        }
        return time/collectList.size();
    }

}
