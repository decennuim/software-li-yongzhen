import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Counter {
    // 服务客户数
    public int customerNum;
    // 提供服务类型数Map
    public Map<Integer, Integer> serviceTypeNumMap;

    Counter() {
        this.serviceTypeNumMap = new HashMap<>(4);
    }

    public String toStr() {
        String serviceTypeNum = "";
        for (Map.Entry<Integer, Integer> entry : serviceTypeNumMap.entrySet()) {
            serviceTypeNum = serviceTypeNum + Service.getService(entry.getKey()).getTypeName() + ":" + entry.getValue() + "次 ";
        }
        return "服务客户数:" + customerNum + "；提供的服务类型和类型数：" + serviceTypeNum;
    }

    public void inc(List<Service> services) {
        cinc();
        sinc(services);
    }

    /**
     * 提供服务数自增
     */
    private void cinc() {
        customerNum++;
    }

    /**
     * 提供服务类型数
     */
    private void sinc(List<Service> services) {
        Integer typeNum = null;
        for (Service s : services) {
            typeNum = serviceTypeNumMap.get(s.getType());
            if (typeNum == null)
                serviceTypeNumMap.put(s.getType(), 1);
            else
                serviceTypeNumMap.put(s.getType(), typeNum + 1);

        }
    }

    public int getCustomerNum() {
        return customerNum;
    }
}

