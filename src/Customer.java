
import java.util.List;
import java.util.Random;

public class Customer {
    // 编号

    public Integer num;
    // 到达时间
    public String enterTime;
    public int needtime;
    // 办理完成后时间
    public String finshTime;
    // 待办理的业务
    public List<Service> services;

    public int getNeedtime() {
        return needtime;
    }

    public void setNeedtime(int needtime) {
        this.needtime = needtime;
    }

    // 服务窗口 默认 1， 2， 3， 4
    public Integer counter;

    public Customer(Integer num, String enterTime, List<Service> services) {
        this.num = num;
        this.enterTime = enterTime;
        this.services = services;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getEnterTime() {
        return enterTime;
    }

    public String getFinshTime() {
        return finshTime;
    }

    public void setFinshTime(String finshTime) {
        this.finshTime = finshTime;
    }

    public List<Service> getServices() {
        return services;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "Customer [num=" + num + ", enterTime=" + TIME.Time1()+", services="
                + services + ", counter=" + counter + "]";
    }


}