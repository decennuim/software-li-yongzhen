public enum Service {
    DEPOSIT(0, "存款", 20), WITHDRAW(1, "取款", 10), LOSS(2, "挂失", 30), REPAY(3, "还贷", 30);

    public int type;
    // 业务类型
    public String typeName;
    // 用时 单位秒
    public int time;

    private Service(int type, String typeName, int time) {
        this.type = type;
        this.typeName = typeName;
        this.time = time;
    }

    public static Service getService(int type) {
        for (Service s : Service.values()) {
            if (type == s.getType()) {
                return s;
            }
        }
        return null;
    }

    public int getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public int getTime() {
        return time;
    }
}
