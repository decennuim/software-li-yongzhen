import java.text.SimpleDateFormat;

public  class TimeFormat {
    private static SimpleDateFormat hmm = new SimpleDateFormat("HH:mm:ss");// 时分秒格式
    private static SimpleDateFormat m = new SimpleDateFormat("ss");// 秒

    public static String format(long time) {
        return hmm.format(time);
    }

    public static String format_m(long time) {
        return m.format(time);
    }

}
